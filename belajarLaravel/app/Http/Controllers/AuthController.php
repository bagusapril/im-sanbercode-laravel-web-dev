<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar()
    {
        return view('page.form');
    }

    public function kirim(Request $request)
        {
            // dd($request ->all());
            $namaLengkap = $request['namaLengkap'];
            $usiaa = $request['usiaa'];

            return view('page.home', ["namaLengkap" => $namaLengkap, 'usiaa' => $usiaa]);
        }
    
}
