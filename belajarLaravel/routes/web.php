<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'dashboard']);
Route::get('/daftar', [AuthController::class, 'daftar']);
Route::post('/kirim', [AuthController::class, 'kirim']);

Route::get('/data-tables', function(){
    return view('page.datatable');
});

Route::get('/tables', function(){
    return view('page.table');
});

// // testing
// Route::get('/master', function(){
//     return view('layout.master');
// });