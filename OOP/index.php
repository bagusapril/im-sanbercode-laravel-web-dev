<?php

require_once ('animal.php');
require_once ('frog.php');
require_once ('ape.php');

$sheep = new Animal("shaun");

echo "Nama Hewan : ".  $sheep->name . "<br>"; // "shaun"
echo "Jumlah KAki : " . $sheep->legs . "<br>"; // 4
echo "Cold blooded: " . $sheep->cold_blooded . "<br> <br>"; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

$kodok = new Frog("buduk");
echo "Nama Hewan : ".  $kodok->name . "<br>"; // "shaun"
echo "Jumlah KAki : " . $kodok->legs . "<br>"; // 4
echo "Cold blooded: " . $kodok->cold_blooded . "<br> "; // "no"

echo "Jump :" . $kodok->jump ."<br> <br>";

$sungokong = new Ape("kera sakti");
echo "Nama Hewan : ".  $sungokong->name . "<br>"; // "shaun"
echo "Jumlah KAki : " . $sungokong->legs . "<br>"; // 4
echo "Cold blooded: " . $sungokong->cold_blooded . "<br> "; // "no"

echo "yell :" . $sungokong->yell ."<br><br>";


?>