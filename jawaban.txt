1. Membuat Database
Jawaban = CREATE DATABASE myshop;

2.Membuat Table di Dalam Database
a. table user
CREATE TABLE `user` (
	`id` INT(11) AUTO_INCREMENT PRIMARY KEY, 
    `name` VARCHAR(255) NOT NULL , 
    `email` VARCHAR(255) NOT NULL , 
    `password` VARCHAR(255) NOT NULL
    );

b. table categori
CREATE TABLE `categori` (
	`id` INT(11) AUTO_INCREMENT PRIMARY KEY, 
    `name` VARCHAR(255) NOT NULL 
    );

c. table items
CREATE TABLE `items` (
	`id` INT(11) AUTO_INCREMENT PRIMARY KEY, 
    `name` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255) NOT NULL,
    `price` INT(11) NOT NULL,
    `stock` INT(11) NOT NULL,
    `category_id` INT(11) NOT NULL,
    FOREIGN KEY (category_id) REFERENCES categori(id)
    );

3.Memasukkan Data pada Table
a. table user
INSERT INTO `user` (`id`, `name`, `email`, `password`) VALUES 
('1234567888', 'John Doe', 'john@doe.com', 'john123'), 
('1234567889', 'Jane Doe', 'jane@doe.com', 'jenita123')

b. table categori
INSERT INTO `categori` (`id`,`name`) VALUES
('1','gadget'),
('2','cloth'),
('3','men'),
('4','women'),
('5','branded')

c. table items
INSERT INTO `items` (`id_items`,`name`,`description`,`price`,`stock`,`category_id`) VALUES
('11','Sumsang b50','hape keren merek sumsang','4000000','100','1'),
('12','Uniklooh','baju keren dari brand ternama','500000','50','2'),
('13','IMHO Watch','jam tangan anak yang jujur banget','2000000','10','1')

4. Mengambil Data dari Database
a.Mengambil data users
SELECT id,name,email FROM `user`

b.1 Mengambil data items untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).
SELECT * FROM `items` WHERE price > 1000000;

b.2 Mengambil data items untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).
SELECT * FROM `items` WHERE name LIKE '%sang%';

c. Menampilkan data items join dengan kategori
SELECT items.* , categori.name FROM categori INNER JOIN items ON items.category_id = categori.id;

5. Mengubah Data dari Database
UPDATE `items` SET `price` = '2500000' WHERE `items`.`id_items` = 11;

